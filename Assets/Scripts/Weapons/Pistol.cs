﻿using UnityEngine;
using System.Collections;

public class Pistol : Weapon {

    public int magazineSize;
    protected int currentAmmo = 0;
    protected static int overallAmmo = 0;
    public float reloadTime = 0.0f;

    // Use this for initialization
    void Awake () {
        overallAmmo = 30;
        currentAmmo = magazineSize;
	}
	
	// Update is called once per frame
	new void Update () {
        base.Update();
	}

    public override void Fire()
    {
        // If there is no ammo in the clip, then immediately reload
        if (currentAmmo == 0 && overallAmmo > 0)
        {
            Reload();
            return;
        }

        // If we are out of ammo, then we cannot fire the weapon
        if (overallAmmo == 0)
        {
            Debug.Log("Out of ammo");
            return;
        }

        if (elapsedTime >= fireRate)
        {
            Instantiate(bullet, bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.rotation);
            elapsedTime = 0.0f;
            currentAmmo--;
            overallAmmo--;
            Debug.Log(overallAmmo);
        }
    }

    public override void Reload()
    {
        Debug.Log("Reloading");
        // Forget reload time for the moment

        if (overallAmmo < magazineSize)
        {
            currentAmmo = overallAmmo;
        }
        else
        {
            currentAmmo = magazineSize;
        }

        Debug.Log("Reloaded");
    }

    public override void IncreaseAmmo(int value)
    {
        Debug.Log("Ammo Increased");
        // Only increase the overall ammo, not the ammo already in the clip
        overallAmmo += value;
        Debug.Log("We now have " + overallAmmo + " rounds now!");
    }
}
