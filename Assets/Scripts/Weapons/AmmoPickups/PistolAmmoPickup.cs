﻿using UnityEngine;
using System.Collections;

public class PistolAmmoPickup : MonoBehaviour
{
    public float rotateSpeed = 30.0f;
    public int value = 5;

    void Start()
    {

    }

    void Update()
    {
        // Will need to fix later once I have models
        transform.Rotate(new Vector3(0f, rotateSpeed * Time.deltaTime, 0f));
    }

    void OnTriggerEnter(Collider col)
    {
        // Send the message to the weapon manager to increase the ammo;
        WeaponManager.Instance.IncreaseAmmo("Pistol", value);

        // Destroy the object last
        Destroy(gameObject);
    }
}
