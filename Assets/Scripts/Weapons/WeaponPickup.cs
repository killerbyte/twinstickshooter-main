﻿using UnityEngine;
using System.Collections;

public class WeaponPickup : MonoBehaviour
{
    public float rotateSpeed = 30.0f;
    protected string weapon;
    protected bool isColliding = false;
    protected string colName;

	void Start ()
    {
	
	}
	
	void Update ()
    {
        // Will need to fix later once I have models
        transform.Rotate(new Vector3(0f, 0f, rotateSpeed * Time.deltaTime));
	}

    protected void OnTriggerEnter(Collider col)
    {
        isColliding = true;
        colName = col.gameObject.tag;
    }

    protected void OnTriggerExit(Collider col)
    {
        isColliding = false;
        colName = col.gameObject.tag;
    }
}
