﻿using UnityEngine;

public class AssaultRifle : Weapon
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
    }

    public override void Fire()
    {
        if (elapsedTime >= fireRate)
        {
            Instantiate(bullet, bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.rotation);
            elapsedTime = 0.0f;
        }
    }
}
