﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    public string gunName = "";

    public float fireRate = 0.0f;
    protected float elapsedTime;

    public Transform bullet;
    public Transform bulletSpawnPoint;

    private bool locked = true;
	
	// Update is called once per frame
	public void Update ()
    {
        elapsedTime += Time.deltaTime;
	}

    public virtual void Fire()
    {

    }

    public virtual void Reload()
    {

    }

    public string GetWeapon()
    {
        return gunName;
    }

    public void Unlock()
    {
        locked = false;
    }

    public bool isLocked()
    {
        return locked;
    }

    public virtual void IncreaseAmmo(int value)
    {
        
    }
}
