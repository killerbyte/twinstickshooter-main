﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    // Create as a singleton
    static GameManager instance = null;
    public static GameManager Instance { get { return instance; } }

    public float elapsedTime;

    public PlayerCharacter player;

    private bool gameLoaded = false;

    public int playerCount;

    // Use this for initialization
    void Start () {
        instance = this;
        elapsedTime = 0.0f;
        playerCount = 0;
    }
	
	// Update is called once per frame
	void Update () {
        elapsedTime += Time.deltaTime;

        // Make sure all Managers are loaded before the player is spawned
        if (!gameLoaded)
        {
            if (UIManager.Instance.isLoaded && WeaponManager.Instance.isLoaded)
            {
                // Load the player instance
                player = Instantiate(player);
                player.name = "Player" + playerCount;

                playerCount++;

                // Load enemy
            }

            gameLoaded = true;
        }
    }
}
