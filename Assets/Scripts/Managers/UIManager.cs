﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Rewired;

public class UIManager : MonoBehaviour
{
    // Create as a singleton
    static UIManager instance = null;
    public static UIManager Instance { get { return instance; } }

    // UI Panels to swap between
    private GameObject UIPausedMenu;
    private GameObject UIOptionsMenu;
    public string inputType;

    // Pause Flag
    public bool isPaused = false;

    public bool isLoaded = false;

    // Event System
    EventSystem system;

    public float menuElapsedTime = 0.0f;

    // Use this for initialization
    void Awake ()
    {
        instance = this;
        gameObject.GetComponentInChildren<Canvas>().enabled = false;

        // Retrieve UI Panels
        UIPausedMenu = GameObject.Find("PauseMenu");
        UIOptionsMenu = GameObject.Find("OptionsMenu");

        system = EventSystem.current;

        if (ReInput.players.GetPlayer(0).controllers.Joysticks.Count == 1)
        {
            inputType = "Controller";
        }
        else
        {
            inputType = "Keyboard/Mouse";
        }

        isLoaded = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isPaused)
        {
            menuElapsedTime += Time.unscaledDeltaTime;
        }
    }

    public void TogglePauseMenu()
    {
        // Not optimal
        if (gameObject.GetComponentInChildren<Canvas>().enabled)
        {
            gameObject.GetComponentInChildren<Canvas>().enabled = false;
            Time.timeScale = 1.0f;
            isPaused = false;
        }
        else
        {
            gameObject.GetComponentInChildren<Canvas>().enabled = true;
            MoveToPause();
            Time.timeScale = 0.0f;
            isPaused = true;
        }
    }
    
    public void Quit()
    {
        Application.Quit();
    }

    public void MoveToOptions()
    {
        UIPausedMenu.SetActive(false);
        UIOptionsMenu.SetActive(true);
        system.SetSelectedGameObject(GameObject.Find("txtMusicVol"));
    }

    public void MoveToPause()
    {
        UIOptionsMenu.SetActive(false);
        UIPausedMenu.SetActive(true);
        system.SetSelectedGameObject(GameObject.Find("btnResume"));
    }

    public void AcceptOptionChanges()
    {
        inputType = GameObject.Find("txtOption").GetComponent<Text>().text;
        print(inputType);
    }
}
