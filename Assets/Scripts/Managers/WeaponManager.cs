﻿using UnityEngine;
using System.Collections;

public class WeaponManager : MonoBehaviour {

    // Create as a singleton
    static WeaponManager instance = null;
    public static WeaponManager Instance { get { return instance; } }

    public bool isLoaded = false;

    [SerializeField]
    private Weapon[] weapons;

    private int currentWeapon = 0;

    private const int START_WEAPON = 0;

	// Use this for initialization
	void Start () {
        instance = this;

        isLoaded = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public Weapon ChangeWeapons(bool left)
    {
        do
        {
            if (left)
            {
                if (currentWeapon > 0)
                    currentWeapon--;
                else
                    currentWeapon = weapons.Length - 1;
            }
            else
            {
                currentWeapon = (currentWeapon + 1) % weapons.Length;
            }
        }
        while (!weapons[currentWeapon].isLocked());

        return weapons[currentWeapon];
    }

    public Weapon ReturnStartingWeapon()
    {
        return weapons[START_WEAPON];
    }

    public void IncreaseAmmo(string weapon, int value)
    {
        for (int i = 0; i < weapons.Length; i++)
        {
            if (weapons[i].GetWeapon() == weapon)
            {
                weapons[i].IncreaseAmmo(value);
            }
        }
    }
}
