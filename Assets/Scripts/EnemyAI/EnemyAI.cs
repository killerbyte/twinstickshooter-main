﻿using UnityEngine;
using System.Collections;

public class EnemyAI : Character {

    public enum EnemyAIState
    {
        Idle, // Like a patrol mode
        Aware, // Actively searching for a player
        Intrigued, // Searching a area. Moves to alert if player is found
        Alert, // Hunting down players. Moves to aggressive if in range of player
        Aggressive, // Engaged with players. Moves to fleeing if low on health
        Fleeing, // Run away
        Dead // Death animation before object destruction
    }

    private Transform player;

    public float stopRange = 500.0f;

	// Use this for initialization
	new void Start ()
    {
        health = 100;

        nav = GetComponent<NavMeshAgent>();
        if (!nav)
            print("Nav Mesh Agent not found");

        player = GameObject.FindGameObjectWithTag("Player").transform;
        if (!player)
            print("Player not found!");

        base.Start();
	}
	
	// Update is called once per frame
	new void Update ()
    {
        //print(Vector3.Distance(transform.position, player.position));
        if (Vector3.Distance(transform.position, player.position) >= stopRange)
        {
            nav.SetDestination(player.position);
        }
        else
        {
            nav.Stop();
            //Fire();
        }

        //elapsedTime += Time.deltaTime;
        base.Update();
    }

    void FixedUpdate()
    {
        
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, stopRange);
    }
}
