﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Rewired;

public class UICarousel : MonoBehaviour
{

    public string[] options;
    public Text titleText;
    public Text optionText;
    public Button leftArrow;
    public Button rightArrow;
    private int currentOption;
    private float changeRate = 0.3f;
    private float elapsedTime = 0.0f;

	// Use this for initialization
	void Start ()
    {
        if (ReInput.players.GetPlayer(0).controllers.Joysticks.Count == 1)
        {
            currentOption = 1;
        }
        else
        {
            currentOption = 0;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Update the text first
        optionText.text = options[currentOption];

        if (EventSystem.current.currentSelectedGameObject == titleText.gameObject)
        {
            bool fireEvent = elapsedTime > changeRate;

            // Check for controller input here
            if (ReInput.players.GetPlayer(0).GetAxis("Move Horizontal") == 1)
            {
                // Move Right
                if (fireEvent)
                    ChangeOption(false);
            }
            else if (ReInput.players.GetPlayer(0).GetAxis("Move Horizontal") == -1)
            {
                // Move Left
                if (fireEvent)
                    ChangeOption(true);
            }
        }

        elapsedTime += Time.unscaledDeltaTime;
    }

    public string GetCurrentOption()
    {
        return options[currentOption];
    }

    public void ChangeOption(bool left)
    {
        if (left)
        {
            currentOption--;
        }
        else
        {
            currentOption++;
        }

        if (currentOption == -1)
        {
            currentOption = options.Length - 1;
        }
        else if (currentOption == options.Length)
        {
            currentOption = 0;
        }

        elapsedTime = 0.0f;
    }
}
