﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Character : MonoBehaviour {

    public GameObject weaponAnchorPoint;

    // Health
    protected int health;

    // Nav Mesh Agent if an AI is controlling this character
    protected NavMeshAgent nav;

    protected float elapsedTime;

    // Fire rate
    public float fireRate;

    // Current Weapon
    protected Weapon currentWeapon;
    protected GameObject currentWeaponObject;
    protected string currentWeaponName;

    // Use this for initialization
    protected void Start ()
    {
        elapsedTime = 0.0f;
	}
	
	// Update is called once per frame
	protected void Update ()
    {
        elapsedTime += Time.deltaTime;
	}

    void FixedUpdate()
    {
        
    }

    public void ApplyDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
