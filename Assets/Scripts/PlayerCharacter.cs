﻿using UnityEngine;
using Rewired;
using System;

[RequireComponent(typeof(CharacterController))]
public class PlayerCharacter : Character
{
    // Speed variables
    public float playerSpeed = 2.0f;
    public float rotationSpeed = 2.0f;

    // Rewired Player
    private Player player; 

    // Input Variables
    private Vector3 movement = new Vector3();
    private CharacterController cc;
    private bool fire;
    private Vector3 rotation = new Vector3();
    private bool paused;
    public bool controller;
    private bool weaponLeft;
    private bool weaponRight;

    // Controller Maps
    ControllerMap mouseMap;
    ControllerMap keyboardMap;
    ControllerMap joystickMap;

    // Use this for initialization
    new void Start()
    {
        health = 100;

        player = ReInput.players.GetPlayer(0);

        cc = GetComponent<CharacterController>();

        fireRate = 0.1f;

        // Retrieve input maps from Rewired
        mouseMap = player.controllers.maps.GetMap(ControllerType.Mouse, 0, 1);
        keyboardMap = player.controllers.maps.GetMap(ControllerType.Keyboard, 0, 0);
        joystickMap = player.controllers.maps.GetMap(ControllerType.Joystick, 0, 2);

        //foreach (ControllerMap map in player.controllers.maps.GetAllMaps())
        //{
        //    map.enabled = true; // set the enabled state on the map
        //}

        // Check which input should be used.
        if (player.controllers.Joysticks.Count == 1)
        {
            // By Default, disable the keyboard map if a controller is connected
            keyboardMap.enabled = false;
            mouseMap.enabled = false;
        }
        else
        {
            // If no controller is connected, then ensure that the keyboard are enabled
            keyboardMap.enabled = true;
            mouseMap.enabled = true;
        }

        currentWeapon = WeaponManager.Instance.ReturnStartingWeapon();
        EquipWeapon();

        base.Start();
    }

    // Update is called once per frame
    new void Update()
    {
        GetInput();
        ProcessInput();

        base.Update();
    }

    // Retrieve all input via Rewired.
    private void GetInput()
    {
        // Check which map I should be using
        if (UIManager.Instance.inputType == "Keyboard/Mouse")
        {
            if (joystickMap != null)
                joystickMap.enabled = false;
            keyboardMap.enabled = true;
            mouseMap.enabled = true;
            controller = false;
        }
        else if (UIManager.Instance.inputType == "Controller")
        {
            joystickMap.enabled = true;
            keyboardMap.enabled = false;
            mouseMap.enabled = false;
            controller = true;
        }

        movement.x = player.GetAxis("Move Horizontal");
        movement.z = player.GetAxis("Move Vertical");
        rotation.x = player.GetAxis("Rotate Horizontal");
        rotation.z = player.GetAxis("Rotate Vertical");
        fire = player.GetButton("Fire Weapon");
        paused = player.GetButtonDown("Start Button");
        weaponLeft = player.GetButtonDown("WeaponLeft");
        weaponRight = player.GetButtonDown("WeaponRight");
    }

    private void ProcessInput()
    {
        // If paused, trigger the pause menu in the UIManager
        if (paused)
        {
            print("Paused Button Pressed");
            UIManager.Instance.TogglePauseMenu();
        }

        if (!UIManager.Instance.isPaused)
        {
            // Process Movement event
            if (movement.x != 0.0f || movement.z != 0.0f)
            {
                cc.Move(movement * playerSpeed * Time.deltaTime);
            }

            if (UIManager.Instance.inputType == "Controller")
            {
                ControllerRotation();
            }
            else if (UIManager.Instance.inputType == "Keyboard/Mouse")
            {
                MouseRotation();
            }

            // Process Fire event
            if (fire && currentWeapon != null)
            {
                currentWeaponObject.GetComponent<Weapon>().Fire();
            }

            // Process swap weapons
            if (weaponLeft)
            {
                SwapWeapons(true);
            }
            else if (weaponRight)
            {
                SwapWeapons(false);
            }

            Camera.main.transform.position = new Vector3(cc.transform.position.x, Camera.main.transform.position.y, cc.transform.position.z);
        }
    }

    private void MouseRotation()
    { 
        Vector3 worldPos = Input.mousePosition;
        Vector3 objPos = Camera.main.WorldToScreenPoint(transform.position);
        //print(worldPos);

        // Get deltaX and deltaY
        float deltaX = worldPos.x - objPos.x;
        float deltaY = worldPos.y - objPos.y;

        float angle = Mathf.Atan2(deltaX, deltaY) * Mathf.Rad2Deg;

        Quaternion rot = Quaternion.Euler(new Vector3(0, angle, 0));

        transform.rotation = rot;
    }

    private void ControllerRotation()
    {
        // Process Movement event
        if (movement.x != 0.0f || movement.y != 0.0f)
        {
            cc.Move(movement * playerSpeed * Time.deltaTime);
        }

        // Process Rotation event - figure out smooth rotation
        if (rotation.sqrMagnitude > 0.1f)
            cc.transform.LookAt(transform.position + rotation);

        float angle = Mathf.Atan2(rotation.x, rotation.z) * Mathf.Rad2Deg;

        Quaternion rot = Quaternion.Euler(new Vector3(0, angle, 0)); // Modify if mouse instead of controller

        Quaternion.Slerp(transform.rotation, rot, rotationSpeed * Time.deltaTime);
    }

    public void EquipWeapon()
    {
        Vector3 pos = weaponAnchorPoint.transform.position;
        Quaternion rot = weaponAnchorPoint.transform.rotation;

        GameObject weapon = Instantiate(currentWeapon.gameObject, pos, rot) as GameObject;
        currentWeaponObject = weapon;
        currentWeaponObject.transform.parent = weaponAnchorPoint.transform;
    }

    public void SwapWeapons(bool left)
    {
        Destroy(currentWeaponObject);
        currentWeapon = WeaponManager.Instance.ChangeWeapons(left);
        EquipWeapon();
    }
}
